package com.wallib.test.dao;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

import com.mongodb.client.result.UpdateResult;
import com.wallib.test.model.RegisterStay;
import com.wallib.test.model.Vehicle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class VehicleDAOImpl implements VehicleDAO {

    @Autowired
    private MongoOperations mongoOps;
    private static final String VEHICLE_COLLECTION = "vehicle";

    private final int NON_RESIDENT_FEE = 65;

    @Override
    public void Create(Vehicle vehicle) {
        Vehicle fetched = Read(vehicle.getPlate());

        if(fetched == null){
            mongoOps.insert(vehicle);
        }
        else if(fetched.getType() != vehicle.getType()){
            log.info("Vehicle already exists. Changing type. This will reset all info about vehicle.");
            Vehicle toReplace = new Vehicle();
            toReplace.setTotal(0);
            toReplace.setId(fetched.getId());
            toReplace.setType(vehicle.getType());
            toReplace.setPlate(fetched.getPlate());
            toReplace.setStays(new ArrayList<Vehicle.Stay>());
            Update(toReplace);
        }
        else{
            log.info("Vehicle already exists in database");
        }
    }

    @Override
    public Vehicle Read(String plate) {
        Query query = new Query(Criteria.where("plate").is(plate));
        return mongoOps.findOne(query, Vehicle.class, VEHICLE_COLLECTION);
    }

    @Override
    public void Update(Vehicle vehicle) {
        mongoOps.save(vehicle, VEHICLE_COLLECTION);
    }

    @Override
    public RegisterStay AddStayEntryDate(String plate, LocalDateTime entryDateTime){
        Query query = new Query(Criteria.where("plate").is(plate));
        Vehicle vehicle = mongoOps.findOne(query, Vehicle.class, VEHICLE_COLLECTION);
        RegisterStay registerStay = null;
        if(vehicle == null){
            Vehicle newVehicle = new Vehicle();
            Vehicle.Stay stay = newVehicle.new Stay(entryDateTime);
            newVehicle.setType(2);
            newVehicle.setPlate(plate);
            newVehicle.setStays(new ArrayList<>(Arrays.asList(stay)));
            Create(newVehicle);
            registerStay = new RegisterStay(stay);
        }
        else{
            ArrayList<Vehicle.Stay> stays = vehicle.getStays();
            if(stays == null){
                vehicle.setStays(new ArrayList<>(Arrays.asList(vehicle.new Stay(entryDateTime))));
                Update(vehicle);
                Vehicle.Stay stay = vehicle.getStays().get(vehicle.getStays().size() - 1);
                registerStay = new RegisterStay(stay);
            }
            else if(stays.size()  > 0 && stays.get(stays.size() - 1).exit == null){
                registerStay = new RegisterStay("ERROR: there's no exit registered for the last entry. Ignoring call...");
            }
            else{
                vehicle.getStays().add(vehicle.new Stay(entryDateTime));
                Update(vehicle);
                Vehicle.Stay stay = vehicle.getStays().get(vehicle.getStays().size() - 1);
                registerStay = new RegisterStay(stay);
            }
        }

        return registerStay;
    }

    @Override
    public RegisterStay AddStayExitDate(String plate, LocalDateTime exitDateTime){
        Query query = new Query(Criteria.where("plate").is(plate));
        Vehicle vehicle = mongoOps.findOne(query, Vehicle.class, VEHICLE_COLLECTION);

        RegisterStay stay = null;
        if(vehicle == null){
            stay = new RegisterStay("ERROR: Vehicle does not exist. Ignoring call...");
        }
        else{
            ArrayList<Vehicle.Stay> stays = vehicle.getStays();
            if(stays == null){
                stay = new RegisterStay("ERROR: Vehicle has no registered entry. Ignoring call...");
            }
            else if(stays.size() == 0 || stays.get(stays.size() - 1).exit != null){
                stay = new RegisterStay("ERROR: there's no registered entry. Ignoring call...");
            }
            else{
                vehicle.getStays().get(vehicle.getStays().size() - 1).setExit(exitDateTime);
                stay = new RegisterStay(vehicle.getStays().get(vehicle.getStays().size() - 1));
    
                // Add total time stayed
                if(vehicle.getType() == 1){
                    vehicle.setTotal(vehicle.getTotal() + stay.getTotalMin());
                }
                else if(vehicle.getType() == 2){
                    stay.setToPay(Long.toString(stay.getTotalMin() * NON_RESIDENT_FEE));
                    vehicle.setTotal(vehicle.getTotal() + stay.getTotalMin());
                }
                Update(vehicle);
            }
        }
        return stay;
    }

    public long RestartOfficialVehiclesStays(){
        Query query = new Query(Criteria.where("type").is(0));
        Update update = new Update();
        update.set("stays", new ArrayList<Vehicle.Stay>());
        UpdateResult result = mongoOps.updateMulti(query, update, Vehicle.class);
        return result.getModifiedCount();
    }

    public long RestartResidentTimeCount(){
        Query query = new Query(Criteria.where("type").is(1));
        Update update = new Update();
        update.set("total", 0);
        UpdateResult result = mongoOps.updateMulti(query, update, Vehicle.class);
        return result.getModifiedCount();
    }

    public ArrayList<Vehicle> GetResidents(){
        ArrayList<Vehicle> residents = new ArrayList<Vehicle>();
        Query query = new Query(Criteria.where("type").is(1));
        residents = new ArrayList<Vehicle>(mongoOps.find(query, Vehicle.class));
        return residents;
    }
}
