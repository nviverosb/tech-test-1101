package com.wallib.test.dao;

import java.time.LocalDateTime;
import java.util.ArrayList;

import com.wallib.test.model.RegisterStay;
import com.wallib.test.model.Vehicle;

public interface VehicleDAO {
    
    public void Create(Vehicle vehicle); 

    public Vehicle Read(String plate);

    public void Update(Vehicle vehicle);

    public RegisterStay AddStayEntryDate(String plate, LocalDateTime dateTime);

    public RegisterStay AddStayExitDate(String plate, LocalDateTime dateTime);

    public long RestartOfficialVehiclesStays();

    public long RestartResidentTimeCount();

    public ArrayList<Vehicle> GetResidents();
}
