package com.wallib.test.controller;

import java.time.LocalDateTime;

import com.wallib.test.model.RegisterStay;
import com.wallib.test.service.StaysService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/wallib/stay")
public class StaysController {

    @Autowired
    StaysService service;

    @PostMapping(value = "/registerEntry/{plate}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RegisterStay> registerEntry(@PathVariable(value = "plate") String plate){
        return service.registerEntry(plate, LocalDateTime.now());
    }

    @PostMapping(value = "/registerExit/{plate}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RegisterStay> registerEXit(@PathVariable(value = "plate") String plate){
        return service.registerExit(plate, LocalDateTime.now());
    }
}
