package com.wallib.test.controller;

import com.wallib.test.model.RegisterVehicle;
import com.wallib.test.model.Vehicle;
import com.wallib.test.service.VehicleService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/wallib/vehicle")
@RestController
public class VehicleController {

    @Autowired
    VehicleService service;
    
    @PostMapping(value = "/registerOfficial", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Vehicle> registerEntry(@RequestBody RegisterVehicle vehicle){
        return service.registerOfficial(vehicle.getPlate());
    }

    @PostMapping(value = "/registerResident", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Vehicle> registerResident(@RequestBody RegisterVehicle vehicle){
        return service.registerResident(vehicle.getPlate());
    }
}
