package com.wallib.test.model;

import lombok.Data;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.ArrayList;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@ToString
@Document(collection = "vehicle")
public class Vehicle {

    @PersistenceConstructor
    public Vehicle(String plate, int type, ArrayList<Stay> stays, long total){
        this.plate = plate;
        this.type = type;
        this.stays = stays;
        this.total = total;
    }

    public Vehicle(){
        this.id = null;
        this.plate = null;
    }
    
    @Id
	private String id;
    private String plate;
    private int type;
    private ArrayList<Stay> stays;
    private long total;

    @Data
    @ToString
    public class Stay{

        @PersistenceConstructor
        public Stay(){
            this.entry = null;
            this.exit = null;
        }

        public Stay(LocalDateTime entry){
            this.entry = entry;
            this.exit = null;
        }

        public Stay(LocalDateTime entry, LocalDateTime exit){
            this.entry = entry;
            this.exit = exit;
        }

        public LocalDateTime entry;
        public LocalDateTime exit;
    }
}
