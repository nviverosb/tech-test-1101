package com.wallib.test.model;

import lombok.Data;

@Data
public class ResidentTimeReport {

    public String plate;
    public long total_time;
    public float amount;

    private final int RESIDENT_FEE = 8;

    public ResidentTimeReport(Vehicle vehicle){
        this.plate = vehicle.getPlate();
        this.total_time = vehicle.getTotal();
        this.amount = vehicle.getTotal() * RESIDENT_FEE;
    }

    @Override
    public String toString(){
        return "" + plate + " ," + total_time + " ," + amount;
    }
}
