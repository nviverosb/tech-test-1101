package com.wallib.test.model;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import lombok.Data;

@Data
public class RegisterStay {

    public RegisterStay(Vehicle.Stay stay){
        this.totalMin = stay.getEntry() != null && stay.getExit() != null ? 
            ChronoUnit.MINUTES.between(stay.entry, stay.exit) :
            0;
        this.exitDate = stay.exit;
        this.entryDate = stay.entry;
    }

    public RegisterStay(Vehicle.Stay stay, long totalMin){
        this.totalMin = totalMin;
        this.exitDate = stay.exit;
        this.entryDate = stay.entry;
    }

    public RegisterStay(String error){
        this.error = error;
    }
    
    private LocalDateTime entryDate;
    private LocalDateTime exitDate;
    private String error;
    private long totalMin;
    private String toPay;
}
