package com.wallib.test.model;

import lombok.Data;

@Data
public class RegisterVehicle {

    private String plate;
}
