package com.wallib.test.service;

import java.util.ArrayList;

import com.wallib.test.dao.VehicleDAOImpl;
import com.wallib.test.model.Vehicle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class VehicleService {

    @Autowired
    VehicleDAOImpl vehicleDAO;
    
    public ResponseEntity<Vehicle> registerOfficial(String plate){

        Vehicle vehicle = new Vehicle();
        vehicle.setPlate(plate);
        vehicle.setStays(new ArrayList<Vehicle.Stay>());
        vehicle.setType(0);
        vehicle.setTotal(0);
        vehicleDAO.Create(vehicle);
        return new ResponseEntity<>(vehicle, HttpStatus.OK);
    }

    public ResponseEntity<Vehicle> registerResident(String plate){

        Vehicle vehicle = new Vehicle();
        vehicle.setPlate(plate);
        vehicle.setStays(new ArrayList<Vehicle.Stay>());
        vehicle.setType(1);
        vehicle.setTotal(0);
        vehicleDAO.Create(vehicle);
        return new ResponseEntity<>(vehicle, HttpStatus.OK);
    }
}
