package com.wallib.test.service;

import java.time.LocalDateTime;
import java.util.ArrayList;

import com.wallib.test.dao.VehicleDAOImpl;
import com.wallib.test.model.RegisterStay;
import com.wallib.test.model.ResidentTimeReport;
import com.wallib.test.model.Vehicle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class StaysService {

    @Autowired
    VehicleDAOImpl vehicleDao;
    
    public ResponseEntity<RegisterStay> registerEntry(String plate, LocalDateTime entryDate){

        RegisterStay stay = vehicleDao.AddStayEntryDate(plate, entryDate);
        ResponseEntity<RegisterStay> response = new ResponseEntity<RegisterStay>(stay, HttpStatus.OK);
        return response;
    }

    public ResponseEntity<RegisterStay> registerExit(String plate, LocalDateTime exitDate){

        RegisterStay stay = vehicleDao.AddStayExitDate(plate, exitDate);
        ResponseEntity<RegisterStay> response = new ResponseEntity<RegisterStay>(stay, HttpStatus.OK);
        return response;
    }

    public ResponseEntity<String> restartMonth(){
        long rowsAffected = vehicleDao.RestartOfficialVehiclesStays();
        rowsAffected += vehicleDao.RestartResidentTimeCount();
        return new ResponseEntity<String>("A total of " + rowsAffected + " documents were updated", HttpStatus.OK);
    } 

    public ResponseEntity<String> getResidentsTotal(){
        String csv_encoded = "";
        ArrayList<Vehicle> residents = vehicleDao.GetResidents();
        ArrayList<ResidentTimeReport> time_reports = new ArrayList<ResidentTimeReport>();

        for(Vehicle resident : residents){
            time_reports.add(new ResidentTimeReport(resident));
        }

        csv_encoded += "Num. placa, Tiempo estacionado (min.), Cantidad a pagar\n";
        for(ResidentTimeReport report: time_reports){
            csv_encoded += report.toString() + "\n";
        }
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_TYPE, "text/csv; charset=UTF-8")
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"stays_report.csv\"")
                .body(csv_encoded);
    }
}
