# Wallib technical test

## 1. Folder Structure

The project consists of two folders:

    * test -> Backend
    * wallib-front -> Frontend

Backend is written in Java, using Spring Boot. I'm using Java 8 for that microservice. For the database I chose MongoDB, since there is minimal initial configuration necessary to get started.

FrontEnd is written using ReactJS. It consists of a single view with a `Button` that generates a `.csv` file.

## 2. Build instructions

Simply execute the `docker-compose.yml` file located in this directory and Docker will take care of the rest. Make sure you have an `.env` file located at the same level of the `docker-compose.yml` file, since all environment variables are going to be taken from there.

## 3. API Reference

Please refer to the `swagger.yaml` file located in this directory for the API Reference.

## 4. Usage

As for current configurations, projects are located in the following addresses.

* Frontend: [http://localhost:3000](http://localhost:3000)
* Backend: [http://localhost:9001](http://localhost:9001)

Keep in mind to append proper backend routes.

## Author
- [Nicolas Viveros](https://github.com/viversba)