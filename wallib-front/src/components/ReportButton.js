import React from 'react';
import Button from 'react-bootstrap/Button';

class ReportButton extends React.Component {
    render() {

        return (
            <a href={process.env.REACT_APP_REPORT_URL} target="_blank" rel="noopener noreferrer" download>
                <Button variant="secondary" size="lg" >Generate Report</Button>
            </a>
        );
    }
}

export default ReportButton;